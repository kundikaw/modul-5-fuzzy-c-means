var dataNasabah = [
  {
    umur : 35,
    rek : 3,
  },
  {
    umur : 22,
    rek : 2,
  },
  {
    umur : 60,
    rek : 1,
  },
  {
    umur : 59,
    rek : 1,
  },
  {
    umur : 25,
    rek : 1,
  },
  {
    umur : 37,
    rek : 4,
  },
];

var matriksUik = [];

// variabel sum matrik uik kw1 kw2
var sumk1w = 0;
var sumk2w = 0;
var a,b;

function setup()
{
  isiUik();
  tampilNasabah();
  tampilUik();
}

function acak(a,b)
{
  return float(nf(random(a, b),0,6));
}

function isiUik()
{
  for(var i = 0 ; i < dataNasabah.length ; i++){
    k1 = acak(0,1);
    k2 = acak(0,1);
    k1w = k1 * k1;
    k2w = k2 * k2;
    sumk1w += k1w;
    sumk2w += k2w;
    matriksUik.push({
      k1 : k1,
      k2 : k2,
      k1w : nf(k1w,0,6),
      k2w : nf(k2w,0,6),
    });
  }
}

function tampilNasabah()
{
  console.log("Dataset Nasabah\n");
  for(var q = 0 ; q < dataNasabah.length ; q++)
  {
        console.log("| No Sample : "+q+" | x : "+dataNasabah[q].umur+" | y : "+dataNasabah[q].rek+" |");
  }
  console.log("\n");
}

function tampilUik()
{
  //tampilkan data matriks Uik
  console.log("Membangun matriks uik");
  console.log("| K1 | K2 | K1^W | k2 ^ W |");
  for(var z = 0 ; z < matriksUik.length ; z++)
  {
    console.log("| "+matriksUik[z].k1+" | "+matriksUik[z].k2+" | "+matriksUik[z].k1w+" | "+matriksUik[z].k2w);
  }
  console.log("rata - rata : "+nf(sumk1w,0,6)+" | "+nf(sumk2w,0,6));
  console.log("\n");
}
